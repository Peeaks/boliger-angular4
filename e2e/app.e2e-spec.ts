import { BoligerWebPage } from './app.po';

describe('boliger-web App', () => {
  let page: BoligerWebPage;

  beforeEach(() => {
    page = new BoligerWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
