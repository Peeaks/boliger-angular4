import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import '@angular/material';
import {
  MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdGridListModule,
  MdInputModule, MdProgressSpinnerModule, MdTooltipModule, MdSnackBarModule, MdListModule, MdSelectModule,
  MdSlideToggleModule
} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MdButtonModule,
    MdCardModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdInputModule,
    MdProgressSpinnerModule,
    MdTooltipModule,
    MdSnackBarModule,
    MdListModule,
    MdSelectModule,
    MdSlideToggleModule
  ],
  exports: [
    MdButtonModule,
    MdCardModule,
    MdMenuModule,
    MdToolbarModule,
    MdGridListModule,
    MdInputModule,
    MdProgressSpinnerModule,
    MdTooltipModule,
    MdSnackBarModule,
    MdListModule,
    MdSelectModule,
    MdSlideToggleModule
  ],
  declarations: []
})
export class MaterialDesignModule {
}
