import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../users/user.service";

@Component( {
    selector : 'boliger-top-toolbar',
    templateUrl : './top-toolbar.component.html',
    styleUrls : [ './top-toolbar.component.css' ]
} )
export class TopToolbarComponent implements OnInit {

    token: String;
    loggedIn: boolean;

    constructor( private router: Router, private userService: UserService ) {
        this.loggedIn = false;
    }

    ngOnInit() {
        this.token = localStorage.getItem( 'token' );

        this.userService.loginEvent.subscribe( ( value ) => {
            this.loggedIn = value;
            this.token = localStorage.getItem( 'token' );
        } );

    }

    // Logs out the user
    logout() {
        this.userService.logout();
    }

}
