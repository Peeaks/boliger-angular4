import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import 'hammerjs';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialDesignModule } from "../material-design/material-design.module";
import { ResidencesComponent } from './residences/residences.component';
import { ResidencesListComponent } from './residences/residences-list/residences-list.component';
import { ResidencesListCellComponent } from './residences/residences-list-cell/residences-list-cell.component';
import { ResidenceService } from "./residences/residence.service";
import { TopToolbarComponent } from './top-toolbar/top-toolbar.component';
import { RouterModule, Routes } from "@angular/router";
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from "angular2-google-maps/core";
import { TopSearchBarComponent } from './residences/top-search-bar/top-search-bar.component';
import { FooterComponent } from './footer/footer.component';
import { SelectedResidenceComponent } from './residences/selected-residence/selected-residence.component';
import { JaNejPipe } from './ja-nej.pipe';
import { UserService } from "./users/user.service";
import { RegisterComponent } from './users/register/register.component';
import { LoginComponent } from './users/login/login.component';
import { ErrorPipe } from './error.pipe';
import { TopImageComponent } from './top-image/top-image.component';
import { AuthGuardService } from "./auth/auth-guard.service";
import { ProfileComponent } from './users/profile/profile.component';
import { CreateResidenceComponent } from './residences/create-residence/create-residence.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ChangeProfileComponent } from './users/change-profile/change-profile.component';
import { EditResidenceComponent } from './residences/edit-residence/edit-residence.component';

const appRoutes: Routes = [
    {
        path : '',
        component : ResidencesComponent
    },
    {
        path : 'map',
        component : MapComponent
    },
    {
        path : 'residence/:id',
        component : SelectedResidenceComponent
    },
    {
        path : 'residence-create',
        component : CreateResidenceComponent, canActivate : [ AuthGuardService ]
    },
    {
        path : 'residence-edit/:id',
        component : EditResidenceComponent, canActivate : [ AuthGuardService ]
    },
    {
        path : 'register',
        component : RegisterComponent
    },
    {
        path : 'login',
        component : LoginComponent
    },
    {
        path : 'profile',
        component : ProfileComponent, canActivate : [ AuthGuardService ]
    },
    {
        path : 'change-password',
        component : ChangePasswordComponent, canActivate : [ AuthGuardService ]
    },
    {
        path : 'change-profile',
        component : ChangeProfileComponent, canActivate : [ AuthGuardService ]
    },
    {
        path : '**',
        component : ResidencesComponent
    }
];

@NgModule( {
    declarations : [
        AppComponent,
        ResidencesComponent,
        ResidencesListComponent,
        ResidencesListCellComponent,
        TopToolbarComponent,
        MapComponent,
        TopSearchBarComponent,
        FooterComponent,
        SelectedResidenceComponent,
        JaNejPipe,
        RegisterComponent,
        LoginComponent,
        ErrorPipe,
        TopImageComponent,
        ProfileComponent,
        CreateResidenceComponent,
        ChangePasswordComponent,
        ChangeProfileComponent,
        EditResidenceComponent,
    ],
    imports : [
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        MaterialDesignModule,
        RouterModule.forRoot( appRoutes ),
        AgmCoreModule.forRoot( {
            apiKey : 'AIzaSyAA7cod5jlA0aUbijJNbo20r6ITnJ66j-M'
        } )
    ],
    providers : [
        ResidenceService,
        UserService,
        AuthGuardService
    ],
    bootstrap : [ AppComponent ]
} )
export class AppModule {
}
