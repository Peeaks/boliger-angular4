import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name : 'jaNej'
} )
// Changes a boolean to a string Ja or Nej
export class JaNejPipe implements PipeTransform {

    transform( value: any, args?: any ): any {
        return value ? 'Ja' : 'Nej';
    }

}
