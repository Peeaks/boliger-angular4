import {Address} from "../entities/Address";
import {ResidenceImage} from "../entities/ResidenceImage";

export class CreateResidenceModel {
  Aconto: number;
  Address: Address;
  Animal: boolean;
  Deposit: number;
  Description: string;
  Energy: String;
  Furnished: boolean;
  Images: String[];
  PrepaidRent: number;
  Rent: number;
  RentalPeriod: string;
  Rooms: number;
  Size: number;
  TakeoverDate: string;
  Title: string;
  Type: string;
}

export class EditResidenceModel {
  Id: number;
  Aconto: number;
  Address: Address;
  Animal: boolean;
  Deposit: number;
  Description: string;
  Energy: String;
  Furnished: boolean;
  PrepaidRent: number;
  Rent: number;
  RentalPeriod: string;
  Rooms: number;
  Size: number;
  TakeoverDate: string;
  Title: string;
  Type: string;
}
