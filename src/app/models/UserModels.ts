import { Address } from '../entities/Address';

export class RegisterUserModel {
    Email: String;
    Password: String;
    ConfirmPassword: String;
    FirstName: String;
    LastName: String;
    Address: Address;
    PhoneNumber: String;
}

export class LoginUserModel {
    Email: String;
    Password: String;
}

export class ChangePasswordModel {
    OldPassword: String;
    NewPassword: String;
    ConfirmPassword: String;
}

export class ChangeProfileModel {
    Id: String;
    FirstName: String;
    LastName: String;
    PhoneNumber: String;
    Address: Address;
}