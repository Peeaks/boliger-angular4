import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ResidenceService } from "../residences/residence.service";
import { Residence } from "../entities/Residence";

@Component( {
    selector : 'boliger-map',
    templateUrl : './map.component.html',
    styleUrls : [ './map.component.css' ]
} )
export class MapComponent implements OnInit {

    residences: Observable<Residence[]>;
    selectedResidence: Residence;
    isSelected: boolean;

    // Default latitude and longitude for centering the map on Denmark
    lat: number = 55.878418;
    lng: number = 9.209007;

    constructor( private residenceService: ResidenceService ) {
        this.residences = residenceService.getResidences();
        this.selectedResidence = new Residence();
        this.isSelected = false;
    }

    ngOnInit() {
    }

    // Called when a Residence Pin is clicked on the map
    changeSelectedResidence( residence ) {
        this.selectedResidence = residence;
        this.isSelected = true;
    }

    // Called when the search input changes
    search( input ) {
        this.residences = this.residenceService.searchResidences( input );
    }

}
