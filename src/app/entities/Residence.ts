import {Address} from './Address';
import {ResidenceImage} from './ResidenceImage';
import {Position} from './Position';
export class Residence {
  Id: number;
  Aconto: number;
  Active: boolean;
  Address: Address;
  Animal: boolean;
  CreationDate: Date;
  Deposit: number;
  Description: string;
  Energy: string;
  Furnished: boolean;
  Images: ResidenceImage[];
  PrepaidRent: number;
  Rent: number;
  RentalPeriod: string;
  Rooms: number;
  Size: number;
  TakeoverDate: string;
  Title: string;
  Type: string;
  UserId: string;
  Position: Position;
}
