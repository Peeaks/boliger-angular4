export class Address {
  Id: number;
  City: String;
  Street: String;
  Zip: Number;
}
