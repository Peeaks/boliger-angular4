import { Address } from "./Address";
import { Residence } from "./Residence";
export class User {
    Id: String;
    Email: String;
    FirstName: String;
    LastName: String;
    Address: Address;
    PhoneNumber: String;
    Residences: Residence[];
}