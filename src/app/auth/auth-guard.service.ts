import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

@Injectable()
export class AuthGuardService implements CanActivate {

    token: String;

    constructor( private router: Router ) {
    }

    // Checks if a token is stored in localStorage
    canActivate( router: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
        if ( localStorage.getItem( 'token' ) ) {
            return true;
        }

        this.router.navigate( [ '/login' ], { queryParams : { returnUrl : state.url } } );
        return false;
    }
}
