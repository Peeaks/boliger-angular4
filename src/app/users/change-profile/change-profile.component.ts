import { Component, OnInit } from '@angular/core';
import { ChangeProfileModel } from "../../models/UserModels";
import { Address } from "../../entities/Address";
import { Router } from "@angular/router";
import { MdSnackBar } from "@angular/material";
import { UserService } from "../user.service";
import { User } from "../../entities/User";

@Component( {
    selector : 'boliger-change-profile',
    templateUrl : './change-profile.component.html',
    styleUrls : [ './change-profile.component.scss' ]
} )
export class ChangeProfileComponent implements OnInit {

    model: ChangeProfileModel;
    isLoading: boolean;
    succesful: boolean;
    errors;

    constructor( private userService: UserService, public snackBar: MdSnackBar, private router: Router ) {
        this.isLoading = false;
        this.succesful = true;

        this.model = new ChangeProfileModel;
        this.model.Address = new Address;

        this.readUser();
    }

    ngOnInit() {
    }

    changeProfile() {
        this.isLoading = true;

        this.userService.changeProfile( this.model ).subscribe( ( succesful ) => {
            this.succesful = true;
            this.isLoading = false;

            if ( this.succesful ) {
                this.snackBar.open( "Dine profil oplysninger er nu opdateret i systemet", "Luk", {
                    duration : 2000,
                } );
                this.router.navigate( [ '/profile' ] );
            }
        }, ( ( response ) => {
            this.succesful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }

    // Loads the logged in Users data and fills in the model when the user is loaded
    private readUser() {
        this.userService.getUserLoggedIn().subscribe( ( user ) => {
            this.model.Id = user.Id;
            this.model.FirstName = user.FirstName;
            this.model.LastName = user.LastName;
            this.model.PhoneNumber = user.PhoneNumber;
            this.model.Address = user.Address;
        }, (( error ) => {
            console.log( error );
        }) );
    }

}
