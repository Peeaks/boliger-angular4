import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from "../entities/User";
import { Subject } from "rxjs/Subject";

@Injectable()
export class UserService {

    private restUrl = 'https://dev.grumsen-it.dk/';

    loginEvent = new Subject<boolean>();

    constructor( private http: Http ) {
    }

    // Returns the user with the sent in id
    getUser( id ): Observable<User> {
        return this.http.get( this.restUrl + 'api/Users/' + id ).map( this.extractData ).catch( this.handleError );
    }

    // Returns the user logged in with the sent in bearer token
    getUserLoggedIn(): Observable<User> {
        const headers = new Headers( {
            'Content-Type' : 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem( 'token' )
        } );
        const options = new RequestOptions( { headers : headers } );

        return this.http.get( this.restUrl + 'api/Account/GetUserLoggedIn', options ).map( this.extractData ).catch( this.handleError );
    }

    // Changes the users password
    changePassword( model ): Observable<boolean> {
        const headers = new Headers( {
            'Content-Type' : 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem( 'token' )
        } );
        const options = new RequestOptions( { headers : headers } );

        return this.http.post( this.restUrl + 'api/Account/ChangePassword', model, options ).map( ( response ) => {
            return response.ok;
        } ).catch( this.handleErrorRegister );
    }

    // Changes a users personal information
    changeProfile( model ): Observable<boolean> {
        const headers = new Headers( {
            'Content-Type' : 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem( 'token' )
        } );
        const options = new RequestOptions( { headers : headers } );

        return this.http.put( this.restUrl + 'api/Users', model, options ).map( ( response ) => {
            console.log( response );
            return response.ok;
        } ).catch( this.handleErrorRegister );
    }

    // Registers a user with the sent in userModel
    register( userModel ): Observable<boolean> {
        const headers = new Headers( { 'Content-Type' : 'application/json' } );
        const options = new RequestOptions( { headers : headers } );

        return this.http.post( this.restUrl + 'api/Account/Register', userModel, options ).map( ( response ) => {
            return response.ok;
        } ).catch( this.handleErrorRegister );
    }

    // Used by the register function to show ModelState errors
    private handleErrorRegister( error: Response | any ) {
        console.log( error );

        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const errors = body[ 'ModelState' ];

            return Observable.throw( errors );
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw( errMsg );
    }

    // Logs in the user and saves the token to localStorage if succesful
    login( userModel ): Observable<boolean> {
        const headers = new Headers( { 'Content-Type' : 'application/x-www-form-urlencoded' } );
        const options = new RequestOptions( { headers : headers } );

        const encoded = 'userName=' + encodeURIComponent( userModel.Email ) +
            '&password=' + encodeURIComponent( userModel.Password ) +
            '&grant_type=password';

        return this.http.post( this.restUrl + 'token', encoded, options ).map( ( response ) => {
            const body = response.json();

            const token = body[ 'access_token' ];

            localStorage.setItem( 'token', token );
            this.loginEvent.next( true );

            return response.ok;
        } ).catch( ( this.handleErrorLogin ) );
    }

    // Removes the saved token from localStorage
    logout(): Observable<boolean> {
        localStorage.removeItem( 'token' );
        this.loginEvent.next( false );
        return Observable.of( true );
    }

    // Extract data from succesful request
    private extractData( res: Response ) {
        return res.json() || {};
    }

    // Extract error message from failed request
    private handleError( error: Response | any ) {
        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify( body );
            errMsg = `${error.status} - ${error.statusText} || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error( errMsg );
        return Observable.throw( errMsg );
    }

    // Used by the login method to read error description from the request if failed
    private handleErrorLogin( error: Response | any ) {
        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const errorDescription = body[ 'error_description' ];

            return Observable.throw( errorDescription );
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw( errMsg );
    }

}
