import { Component, OnInit } from '@angular/core';
import { ChangePasswordModel } from "../../models/UserModels";
import { UserService } from "../user.service";
import { MdSnackBar } from "@angular/material";
import { Router } from "@angular/router";

@Component( {
    selector : 'boliger-change-password',
    templateUrl : './change-password.component.html',
    styleUrls : [ './change-password.component.scss' ]
} )
export class ChangePasswordComponent implements OnInit {

    model: ChangePasswordModel;
    isLoading: boolean;
    succesful: boolean;
    errors;

    constructor( private userService: UserService, private snackBar: MdSnackBar, private router: Router ) {
        this.isLoading = false;
        this.succesful = true;

        this.model = new ChangePasswordModel();
    }

    ngOnInit() {
    }

    changePassword() {
        this.isLoading = true;

        this.userService.changePassword( this.model ).subscribe( ( succesful ) => {
            this.succesful = true;
            this.isLoading = false;

            if ( this.succesful ) {
                this.snackBar.open( "Din kode er nu ændret", "Luk", {
                    duration : 2000,
                } );
                this.router.navigate( [ '/profile' ] );
            }
        }, ( ( response ) => {
            this.succesful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }

}
