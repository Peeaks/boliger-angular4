import { Component, OnInit } from '@angular/core';
import { LoginUserModel } from "../../models/UserModels";
import { UserService } from "../user.service";
import { MdSnackBar } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";

@Component( {
    selector : 'boliger-login',
    templateUrl : './login.component.html',
    styleUrls : [ './login.component.scss' ]
} )
export class LoginComponent implements OnInit {

    userModel: LoginUserModel;
    isLoading: boolean;
    succesful: boolean;
    errors;

    returnUrl: string;

    constructor( private userService: UserService, public snackBar: MdSnackBar, private router: Router, private route: ActivatedRoute ) {
        this.isLoading = false;
        this.succesful = true;

        this.userModel = new LoginUserModel();
    }

    ngOnInit() {
        this.returnUrl = this.route.snapshot.queryParams[ 'returnUrl' ] || '/';
    }

    login() {
        this.isLoading = true;

        this.userService.login( this.userModel ).subscribe( ( succesful ) => {
            this.succesful = true;
            this.isLoading = false;

            if ( this.succesful ) {
                this.snackBar.open( "Du er nu logget ind", "Luk", {
                    duration : 2000,
                } );

                this.router.navigate( [ this.returnUrl ] );
                // location.reload();
            }
        }, ( ( response ) => {
            this.succesful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }

}
