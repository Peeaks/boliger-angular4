import { Component, Input, OnInit } from '@angular/core';
import { UserService } from "../user.service";
import { RegisterUserModel } from "../../models/UserModels";
import { Address } from "../../entities/Address";
import { MdSnackBar } from "@angular/material";
import { Router } from "@angular/router";

@Component( {
    selector : 'boliger-register',
    templateUrl : './register.component.html',
    styleUrls : [ './register.component.scss' ]
} )
export class RegisterComponent implements OnInit {

    userModel: RegisterUserModel;
    isLoading: boolean;
    succesful: boolean;
    errors;

    constructor( private userService: UserService, public snackBar: MdSnackBar, private router: Router ) {
        this.isLoading = false;
        this.succesful = true;

        this.userModel = new RegisterUserModel;
        this.userModel.Address = new Address;
    }

    ngOnInit() {
    }

    register() {
        this.isLoading = true;

        this.userService.register( this.userModel ).subscribe( ( succesful ) => {
            this.succesful = true;
            this.isLoading = false;

            if ( this.succesful ) {
                this.snackBar.open( "Din nye bruger er oprettet i systemet", "Luk", {
                    duration : 2000,
                } );
                this.router.navigate( [ '/login' ] );
            }
        }, ( ( response ) => {
            this.succesful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }
}
