import { Component, OnInit } from '@angular/core';
import { UserService } from "../user.service";
import { User } from "../../entities/User";
import { Residence } from "../../entities/Residence";
import { Address } from "../../entities/Address";
import { ResidenceService } from "../../residences/residence.service";

@Component( {
    selector : 'boliger-profile',
    templateUrl : './profile.component.html',
    styleUrls : [ './profile.component.scss' ]
} )
export class ProfileComponent implements OnInit {

    user: User;

    constructor( private userService: UserService, private residenceService: ResidenceService ) {
        this.user = new User;
        this.user.Address = new Address;

        this.readUser();
    }

    ngOnInit() {
    }

    deleteResidence( residence ) {
        this.residenceService.deleteResidence( residence ).subscribe( ( success ) => {
            this.readUser();
        }, (( error ) => {
            console.log( error );
        }) );
    }

    // Loads the logged in Users data and fills in the model when the user is loaded
    private readUser() {
        this.userService.getUserLoggedIn().subscribe( ( user ) => {
            this.user = user;
        }, (( error ) => {
            console.log( error );
        }) );
    }

}
