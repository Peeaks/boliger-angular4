import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ResidenceService } from "./residence.service";
import { Observable } from "rxjs";
import { Residence } from "../entities/Residence";

@Component( {
    changeDetection : ChangeDetectionStrategy.OnPush,
    selector : 'boliger-residences',
    templateUrl : './residences.component.html',
    styleUrls : [ './residences.component.css' ]
} )

export class ResidencesComponent implements OnInit {

    residences: Observable<Residence[]>;

    isLoading: boolean;

    constructor( private residenceService: ResidenceService ) {
        this.isLoading = true;
        this.residences = residenceService.getResidences().finally( () => this.isLoading = false );
    }

    // Updates the residences array when a search event is received
    search( input ) {
        this.isLoading = true;
        this.residences = this.residenceService.searchResidences( input ).finally( () => this.isLoading = false );
    }

    ngOnInit() {
    }
}
