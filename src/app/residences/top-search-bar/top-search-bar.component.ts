import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component( {
    selector : 'boliger-top-search-bar',
    templateUrl : './top-search-bar.component.html',
    styleUrls : [ './top-search-bar.component.scss' ]
} )
export class TopSearchBarComponent implements OnInit {

    @Output()
    searchResidencesEvent = new EventEmitter<String>();

    // Emits an event when the search input changes
    InputChanged( searchText ) {
        this.searchResidencesEvent.emit( searchText );
    }

    ngOnInit() {
    }
}
