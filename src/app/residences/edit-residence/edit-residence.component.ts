import { Component, OnDestroy, OnInit } from '@angular/core';
import { CreateResidenceModel, EditResidenceModel } from "../../models/ResidenceModels";
import { Address } from "../../entities/Address";
import { ResidenceService } from "../residence.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MdSnackBar } from "@angular/material";

@Component( {
    selector : 'boliger-edit-residence',
    templateUrl : './edit-residence.component.html',
    styleUrls : [ './edit-residence.component.scss' ]
} )
export class EditResidenceComponent implements OnInit, OnDestroy {

    id: number;
    private sub;
    model: EditResidenceModel;
    isLoading: boolean;
    successful: boolean;
    errors;

    // The different types of residences
    types = [
        { value : 'Lejlighed' },
        { value : 'Hus' }
    ];

    // The different types of energy grades
    energy = [
        { value : 'A2020' },
        { value : 'A2015' },
        { value : 'A2010' },
        { value : 'B' },
        { value : 'C' },
        { value : 'D' },
        { value : 'E' },
        { value : 'F' },
        { value : 'G' }
    ];

    constructor( private residenceService: ResidenceService, private route: ActivatedRoute, private router: Router, private snackBar: MdSnackBar ) {
        this.isLoading = false;
        this.successful = true;

        this.model = new EditResidenceModel();
        this.model.Address = new Address();
    }

    ngOnInit() {
        this.router.events.subscribe( ( path ) => {
            window.scrollTo( 0, 0 );
        } );

        this.sub = this.route.params.subscribe( params => {
            this.id = +params[ 'id' ];
            this.readResidence();
        } );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    updateResidence() {
        this.isLoading = true;

        this.residenceService.updateResidence( this.model ).subscribe( ( succesful ) => {
            this.successful = true;
            this.isLoading = false;

            if ( this.successful ) {
                this.snackBar.open( 'Din bolig er nu opdateret', 'Luk', {
                    duration : 2000,
                } );
                this.router.navigate( [ '/profile' ] );
            }
        }, ( ( response ) => {
            this.successful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }

    // Loads the Residence and updates the models data when it is loaded
    private readResidence() {
        this.residenceService.getResidence( this.id ).subscribe( ( residence ) => {
            this.model.Id = residence.Id;
            this.model.Aconto = residence.Aconto;
            this.model.Animal = residence.Animal;
            this.model.Deposit = residence.Deposit;
            this.model.Description = residence.Description;
            this.model.Energy = residence.Energy;
            this.model.Furnished = residence.Furnished;
            this.model.PrepaidRent = residence.PrepaidRent;
            this.model.Rent = residence.Rent;
            this.model.RentalPeriod = residence.RentalPeriod;
            this.model.Rooms = residence.Rooms;
            this.model.Size = residence.Size;
            this.model.TakeoverDate = residence.TakeoverDate;
            this.model.Title = residence.Title;
            this.model.Type = residence.Type;
            this.model.Address = residence.Address;

        }, (( error ) => {
            console.log( error );
        }) );
    }
}
