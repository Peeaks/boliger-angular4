import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Residence } from '../entities/Residence';
import { CreateResidenceModel } from "../models/ResidenceModels";


@Injectable()
export class ResidenceService {

    private restUrl = 'https://dev.grumsen-it.dk/api/Residences';

    constructor( private http: Http ) {
    }

    // Creates a residence on the backend with the sent in CreateResidenceModel
    createResidence( model: CreateResidenceModel ): Observable<boolean> {
        return this.http.post( this.restUrl, model, this.getHeader() ).map( ( response ) => {
            return response.ok;
        } ).catch( this.handleErrorCreate );
    }

    // Reads the Residence with the sent in id
    getResidence( id ): Observable<Residence> {
        return this.http.get( this.restUrl + '/' + id ).map( this.extractData ).catch( this.handleError );
    }

    // Reads all Residences
    getResidences(): Observable<Residence[]> {
        return this.http.get( this.restUrl ).map( this.extractData ).catch( this.handleError );
    }

    // Updates the sent in Residence with the model
    updateResidence( model ): Observable<boolean> {
        return this.http.put( this.restUrl, model, this.getHeader() ).map( ( response ) => {
            return response.ok;
        } ).catch( this.handleErrorCreate );
    }

    // Deletes the sent in Residence
    deleteResidence( residence ): Observable<boolean> {
        return this.http.delete( this.restUrl + '/' + residence.Id, this.getHeader() ).map( ( response ) => {
            return response.ok;
        } ).catch( this.handleError );
    }

    // Returns an array of Residences based on the search criteria
    searchResidences( search ): Observable<Residence[]> {
        return this.http.get( this.restUrl + '/Search?input=' + search ).map( this.extractData ).catch( this.handleError );
    }

    // Returns a header with content-type JSON and the bearer token atached
    private getHeader(): RequestOptions {
        const headers = new Headers( {
            'Content-Type' : 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem( 'token' )
        } );
        return new RequestOptions( { headers : headers } );
    }

    // Used to extract data from a succesful request
    private extractData( res: Response ) {
        return res.json() || {};
    }

    // Used to extract error messages from the failed request
    private handleError( error: Response | any ) {
        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify( body );
            errMsg = `${error.status} - ${error.statusText} || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error( errMsg );
        return Observable.throw( errMsg );
    }

    // Used by the createResidence function to receive ModelState errors from the backend
    private handleErrorCreate( error: Response | any ) {
        console.log( error );

        let errMsg: string;
        if ( error instanceof Response ) {
            const body = error.json() || '';
            const errors = body[ 'ModelState' ];

            return Observable.throw( errors );
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw( errMsg );
    }
}
