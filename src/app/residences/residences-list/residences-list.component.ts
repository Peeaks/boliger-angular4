import {Component, OnInit, Input} from '@angular/core';
import {Observable} from "rxjs";
import {Residence} from "../../entities/Residence";

@Component({
  selector: 'boliger-residences-list',
  templateUrl: 'residences-list.component.html',
  styleUrls: ['residences-list.component.css']
})
export class ResidencesListComponent implements OnInit {


  @Input()
  residences: Observable<Residence[]>;

  @Input()
  isLoading: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
