import { Component, OnInit } from '@angular/core';
import { CreateResidenceModel } from "../../models/ResidenceModels";
import { Address } from "../../entities/Address";
import { ResidenceService } from "../residence.service";
import { isLoop } from "tslint";
import { Router } from "@angular/router";

@Component( {
    selector : 'boliger-create-residence',
    templateUrl : './create-residence.component.html',
    styleUrls : [ './create-residence.component.scss' ]
} )
export class CreateResidenceComponent implements OnInit {

    model: CreateResidenceModel;
    isLoading: boolean
    succesful: boolean;
    errors;

    // The different types of residences
    types = [
        { value : 'Lejlighed' },
        { value : 'Hus' }
    ];

    // The different types of energy grades
    energy = [
        { value : 'A2020' },
        { value : 'A2015' },
        { value : 'A2010' },
        { value : 'B' },
        { value : 'C' },
        { value : 'D' },
        { value : 'E' },
        { value : 'F' },
        { value : 'G' }
    ];

    constructor( private residenceService: ResidenceService, private router: Router ) {
        this.model = new CreateResidenceModel;
        this.model.Address = new Address;
        this.model.Images = new Array<String>();

        this.model.Furnished = false;
        this.model.Animal = false;
    }

    ngOnInit() {
    }

    createResidence() {
        console.log( this.model );

        this.isLoading = true;
        this.residenceService.createResidence( this.model ).subscribe( ( success ) => {
            this.succesful = true;
            this.isLoading = false;

            this.router.navigate( [ '/profile' ] );

        }, ( ( response ) => {
            this.succesful = false;
            this.isLoading = false;
            this.errors = response;
        }) );
    }

    // Called when a image file is loaded into the image input fields
    changeListener( event, number ) {
        this.loadImage( event.target, number );
    }

    // Encodes a image to base64 and adds it as a string to the model
    loadImage( inputValue, number ) {
        let file: File = inputValue.files[ 0 ];
        let myReader: FileReader = new FileReader();

        myReader.onloadend = ( e ) => {
            this.model.Images[ number ] = myReader.result.split( /,(.+)/ )[ 1 ];
        }
        myReader.readAsDataURL( file );
    }

}
