import { Component, OnDestroy, OnInit } from '@angular/core';
import { ResidenceService } from "../residence.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Residence } from "../../entities/Residence";
import { ResidenceImage } from "../../entities/ResidenceImage";
import { Address } from "../../entities/Address";
import { Position } from "../../entities/Position";
import { User } from "../../entities/User";
import { UserService } from "../../users/user.service";

@Component( {
    selector : 'boliger-selected-residence',
    templateUrl : './selected-residence.component.html',
    styleUrls : [ './selected-residence.component.scss' ]
} )
export class SelectedResidenceComponent implements OnInit, OnDestroy {

    id: number;
    private sub: any;
    private residence: Residence;
    private user: User;

    constructor( private route: ActivatedRoute, private router: Router, private residenceService: ResidenceService, private userSerivce: UserService ) {
        this.residence = new Residence();
        this.residence.Address = new Address();
        this.residence.Position = new Position();

        this.user = new User();
    }

    ngOnInit() {

        // Rolls the window to the top
        this.router.events.subscribe( ( path ) => {
            window.scrollTo( 0, 0 );
        } );

        // Getd the sent in paramter from the URL
        this.sub = this.route.params.subscribe( params => {
            this.id = +params[ 'id' ];
        } );


        // Loads the selected residences data
        this.residenceService.getResidence( this.id ).subscribe( ( foundResidence ) => {
            this.residence = foundResidence
            this.userSerivce.getUser( this.residence.UserId ).subscribe( foundUser => this.user = foundUser );
        } );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}