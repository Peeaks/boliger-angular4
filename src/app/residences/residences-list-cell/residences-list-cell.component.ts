import {Component, OnInit, Input} from '@angular/core';
import {Residence} from "../../entities/Residence";

@Component({
  selector: 'boliger-residences-list-cell',
  templateUrl: 'residences-list-cell.component.html',
  styleUrls: ['residences-list-cell.component.css']
})
export class ResidencesListCellComponent implements OnInit {

  @Input()
  residence: Residence;

  constructor() { }

  ngOnInit() {
  }

}
